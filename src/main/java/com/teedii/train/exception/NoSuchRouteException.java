package com.teedii.train.exception;

public class NoSuchRouteException extends Exception {

	@Override
    public String getMessage(){
        return "NO SUCH ROUTE";
    }
}
