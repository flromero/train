package com.teedii.train.run;

import com.teedii.train.exception.NoSuchRouteException;
import com.teedii.trains.model.Graph;
import com.teedii.trains.model.Town;
import com.teedii.trains.process.Routes;

/**
 * App.java
 *   
 * @author TEEDDI S.A.S
 * 
 */
public class App 
{
    public static void main( String[] args )
    {

        Graph graph = new Graph();

		Town  a = new Town("A");
        Town  b = new Town("B");
        Town  c = new Town("C");
        Town  d = new Town("D");
        Town  e = new Town("E");
        
        a.addEdge(b, 5);
        a.addEdge(d, 5);
        a.addEdge(e, 7);
        graph.addTown(a);
        
        b.addEdge(c, 4);
        graph.addTown(b);
        
        c.addEdge(d, 8);
        c.addEdge(e, 2);
        graph.addTown(c);
        
        d.addEdge(c, 8);
        d.addEdge(e, 6);
        graph.addTown(d);
        
        e.addEdge(b, 3);
        graph.addTown(e);
        
        Routes processor = new Routes(graph);
        
        try {
			System.out.println("#1: "+ processor.getDistanceFromRoute("ABC"));
		} catch (NoSuchRouteException ex) {
			System.out.println("#1: "+ ex.getMessage());
		}
        
       
		try {
			System.out.println("#2: "+ processor.getDistanceFromRoute("AD"));
		} catch (NoSuchRouteException ex) {
			System.out.println("#2: "+ ex.getMessage());
		}
        
		try {
			System.out.println("#3: "+ processor.getDistanceFromRoute("ADC"));
		} catch (NoSuchRouteException ex) {
			System.out.println("#3: "+ ex.getMessage());
		}
		
        try {
			System.out.println("#4: "+ processor.getDistanceFromRoute("AEBCD"));
		} catch (NoSuchRouteException ex) {
			System.out.println("#4: "+ ex.getMessage());
		}
        
        try {
			System.out.println("#5: "+ processor.getDistanceFromRoute("AED"));
		} catch (NoSuchRouteException ex) {
			System.out.println("#5: "+ ex.getMessage());
		} 
            
        System.out.println("#6: " + processor.getRoutesWhitinMaxStops("C", "C", 3));
        System.out.println("#7: " + processor.getRoutesExacStops("A", "C", 4));
        System.out.println("#8: " + processor.getRoutesShortes("A", "C"));
        System.out.println("#9: " + processor.getRoutesShortes("B", "B"));
        System.out.println("#10: "+ processor.countLessDistance("C", "C", 30));
    }
    
    
}
