package com.teedii.trains.model;

import java.util.ArrayList;
import java.util.List;
/**
 * Town.java
 *   
 * @author TEEDDI S.A.S
 * 
 */
public class Town {
	
	private String name;
	private List<Edge> listEdge;
	
	public Town(String name) {
		this.name = name;
	}
	
	
	public void addEdge(Town destination, int weight) {
		if(listEdge == null) {
			this.listEdge = new ArrayList<Edge>();
		}
		Edge nEdge = new Edge(this, destination, weight);
		this.listEdge.add(nEdge);
		
	}
	
	public void addEdge(Edge edge) {
		if(listEdge == null) {
			this.listEdge = new ArrayList<Edge>();
		}
		edge.setOrigin(this);
		this.listEdge.add(edge);
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Edge> getListEdge() {
		return listEdge;
	}

	public void setListEdge(List<Edge> listEdge) {
		this.listEdge = listEdge;
	}


	@Override
	public String toString() {
		return "Town [name=" + name + ", listEdge=" + listEdge + "]";
	}


	
	
	
}
