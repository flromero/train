
package com.teedii.trains.model;
/**
 * Edge.java
 *   
 * @author TEEDDI S.A.S
 * 
 */
public class Edge {
	private Town origin;
	private Town destination;
	private int weight;

	
	
	public Edge(Town origin, Town destination, int weight) {
		this.origin = origin;
		this.destination = destination;
		this.weight = weight;
	}

	public Edge(Town origin, Town destination) {
		this.origin = origin;
		this.destination = destination;
	}
	
	public Town getOrigin() {
		return origin;
	}
	public void setOrigin(Town origin) {
		this.origin = origin;
	}
	public Town getDestination() {
		return destination;
	}
	public void setDestination(Town destination) {
		this.destination = destination;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}


	@Override
	public String toString() {
		return "Edge [origin=" + origin.getName() + ", destination=" + destination.getName() + ", weight=" + weight + "]";
	}

	
	
}
