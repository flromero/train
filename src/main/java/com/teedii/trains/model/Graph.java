package com.teedii.trains.model;

import java.util.ArrayList;
import java.util.List;
/**
 * Graph.java
 *   
 * @author TEEDDI S.A.S
 * 
 */
public class Graph {
	private List<Town> listTown;
	
	public void addTown(Town town) {
		if(this.listTown == null) {
			this.listTown = new ArrayList<Town>();
		}
		
		this.listTown.add(town);
	}
	
	

	public List<Town> getListTown() {
		return listTown;
	}



	public void setListTown(List<Town> listTown) {
		this.listTown = listTown;
	}



	@Override
	public String toString() {
		return "Graph [listTown=" + listTown + "]";
	}
	
	
}
