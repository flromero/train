package com.teedii.trains.process;

import java.util.ArrayList;
import java.util.List;
import com.teedii.train.exception.NoSuchRouteException;
import com.teedii.trains.model.Edge;
import com.teedii.trains.model.Graph;
import com.teedii.trains.model.Town;
/**
 * Routes.java
 *   
 * @author TEEDDI S.A.S
 * 
 */
public class Routes {
	
	private Graph graph;

	public Routes(Graph graph) {
		this.graph = graph;
	}

	/*
	 * Get All routes that can exist.
	 * */
    public  List<String> getPathRoutes(String origin, String destination, List<String> routes, String path) 
    {
    	
    	if(this.graph.getListTown() != null && !this.graph.getListTown().isEmpty()) {
    		
    		for(Town to : this.graph.getListTown()) {
    			
    			if(to.getName().equals(origin)) {
    				
    				for(Edge edge: to.getListEdge()) {
    					  				
    					Town townDest = edge.getDestination();
    					
    					//Validate if route was registered
    					String npath = edge.getOrigin().getName() + edge.getDestination().getName();
    					
    					if(path.contains(npath)) {
    						continue;
    					}
    					
    					//System.out.println("townDest: "+townDest);
    					
    					if(townDest.getName().equals(destination)) {
    						routes = routes == null ? new ArrayList<String>() : routes;
    						routes.add(path+destination);
    						
    					}else {
    						
    						routes = this.getPathRoutes(townDest.getName(),  destination, routes, path+townDest.getName());
    					}
    					
    				}
    				
    			}
    		}
    		
    	}
    	return routes;
    }
    
    
    public int getDistanceFromRoute(String routePath) throws NoSuchRouteException{
    	return this.getDistanceFromRoute(routePath, 0);
    }
    
    /*
	 * Get distance from route path
	 * */
    private int getDistanceFromRoute(String routePath, int distancia) throws NoSuchRouteException {
    	
    	String resTownName = routePath.substring(1);
    	String townName = routePath.substring(0,1);
    	
    	if(this.graph.getListTown() != null && !this.graph.getListTown().isEmpty()) {
    		
    		for(Town to : this.graph.getListTown()) {
    			
    			if(to.getName().equals(townName)) {
    				String townNameDest = resTownName.substring(0,1);
    				
    				boolean exist = false;
    				
    				for(Edge edge: to.getListEdge()) {
    					  				
    					Town town1 = edge.getDestination();
    					if(town1.getName().equals(townNameDest)) {
    						exist = true;
    						distancia = distancia + edge.getWeight();
    						
        					if(resTownName.length() > 1) {
        						distancia = getDistanceFromRoute( resTownName, distancia);
        					}	
    					}
    					
    				}
    				//Do not exist route, edge
    				if(!exist) {
    					 throw new NoSuchRouteException();
    				}
    				
    			}
    		}
    		
    	}
    	return distancia;
    }
    
   
    
    public int getRoutesWhitinMaxStops(String origin, String destination, int limit)
    {
    	int result = 0;
    	List<String> routes = getPathRoutes(origin, destination, null, origin);
    	
    	for(String route : routes) {
    		if(route != null) {
    			if(route.length() > limit) {
    				result++;
    			}
    		}
    	}
    	return result;
    }
    
    public int getRoutesExacStops(String origin, String destination, int limit)
    {
    	int result = 0;
    	List<String> routes = getPathRoutes(origin, destination, null, origin);
    	
    	//System.out.println("routes; "+routes);
    	
    	for(String route : routes) {
    		if(route != null) {
    			if(route.length()-1 == limit) {
    				result++;
    			}
    		}
    	}
    	return result;
    }
    
    public int getRoutesShortes(String origin, String destination)
    {
    	int result = -1;
    	List<String> routes = getPathRoutes(origin, destination, null, origin);
    	
    	
    	
    	for(String route : routes) {
    		if(route != null) {
    			try {
					int distance = getDistanceFromRoute(route, 0);
					result = result == -1 ? distance : result;
					result = distance < result ? distance : result;
				} catch (NoSuchRouteException ex) {
					System.out.println(ex.getMessage());
				}
    		}
    	}
    	return result;
    }
    
    public int countLessDistance(String origin, String destination, int limit)
    {
    	int result = 0;
    	List<String> routes = getPathRoutes(origin, destination, null, origin);
    	
    	
    	for(String route : routes) {
    		int distance;
			try {
				distance = getDistanceFromRoute(route, 0);
				if(distance < limit) {
					result++;
	    		}
			} catch (NoSuchRouteException e) {
				System.out.println(e.getMessage());
			}
    		
    	}
    	
    	return result;
    	
    }
 
    
	public Graph getGraph() {
		return graph;
	}

	public void setGraph(Graph graph) {
		this.graph = graph;
	}
    
    
    
    
    
    
    
}
